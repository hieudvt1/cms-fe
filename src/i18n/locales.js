export const LOCALES = {
  ENGLISH: "en-US",
  VIETNAMESE: "vi-VN",
  FRENCH: "fr-FR",
};
