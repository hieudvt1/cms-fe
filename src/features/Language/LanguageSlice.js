import { createSlice } from "@reduxjs/toolkit";

const { LOCALES } = require("~/i18n/locales");

const initialState = {
  language: localStorage.getItem("locale") || LOCALES.ENGLISH,
};

export const languageSlice = createSlice({
  name: "language",
  initialState,
  reducers: {
    changeLanguage: (state, action) => {
      state.language = action.payload;
    },
  },
});

export const { changeLanguage } = languageSlice.actions;
export default languageSlice.reducer;
