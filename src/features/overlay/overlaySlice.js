import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isOpen: false,
};

export const overlaySlice = createSlice({
  name: "overlay",
  initialState,
  reducers: {
    setOverlay: (state, action) => {
      state.isOpen = action.payload;
    },
  },
});

export const { setOverlay } = overlaySlice.actions;
export default overlaySlice.reducer;
