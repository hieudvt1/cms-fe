import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import languageReducer from "~/features/Language/LanguageSlice";
import overlayReducer from "~/features/overlay/overlaySlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    language: languageReducer,
    overlay: overlayReducer,
  },
});
