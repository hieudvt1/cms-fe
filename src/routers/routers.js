import config from "~/config";
import Login from "~/pages/Login";
import ForgotPass from "~/pages/ForgotPass";
import ExchangeRate from "~/pages/ExchangeRate";
import Contract from "~/pages/Contract";
import Inventory from "~/pages/Inventory";
import Invoice from "~/pages/Invoice";
import History from "~/pages/History/History";
import MySetting from "~/pages/MySetting";
import UsersSetting from "~/pages/UsersSetting";

const publicRoutes = [
  { path: config.routes.login, component: Login },
  { path: config.routes.forgotPass, component: ForgotPass },
];

const privateRoutes = [
  { path: config.routes.exchangeRate, component: ExchangeRate },
  { path: config.routes.contract, component: Contract },
  { path: config.routes.inventory, component: Inventory },
  { path: config.routes.invoice, component: Invoice },
  { path: config.routes.history, component: History },
];

const settingsRoutes = [
  { path: config.routes.mySetting, component: MySetting },
  { path: config.routes.usersSetting, component: UsersSetting },
];

export { publicRoutes, privateRoutes, settingsRoutes };
