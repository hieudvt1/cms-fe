import classNames from "classnames/bind";

import styles from "./SortButton.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";

const cx = classNames.bind(styles);

function SortButton({ onClick, direct }) {
  return (
    <div onClick={onClick} className={cx("wrapper")}>
      <FontAwesomeIcon
        icon={faCaretUp}
        className={cx("up", direct === "up" && "color")}
      />
      <FontAwesomeIcon
        icon={faCaretDown}
        className={cx("down", direct === "down" && "color")}
      />
    </div>
  );
}

export default SortButton;
