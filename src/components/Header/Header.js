import classNames from "classnames/bind";

import styles from "./Header.module.scss";
import images from "~/assets/images";
import User from "../User";

const cx = classNames.bind(styles);

function Header() {
  return (
    <div className={cx("wrapper")}>
      <div className={cx("content")}>
        <img src={images.logo} alt="logo" className={cx("logo")} />
        <User />
      </div>
    </div>
  );
}

export default Header;
