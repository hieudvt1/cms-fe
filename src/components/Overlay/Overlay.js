import classNames from "classnames/bind";

import styles from "./Overlay.module.scss";

const cx = classNames.bind(styles);

function Overlay({ className }) {
  return <div className={classNames(className, cx("wrapper"))}></div>;
}

export default Overlay;
