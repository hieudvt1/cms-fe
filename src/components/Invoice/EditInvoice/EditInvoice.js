import { Formik, Form } from "formik";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import Button from "~/components/Button";
import EditInput from "./EditInput";
import styles from "./EditInvoice.module.scss";

const cx = classNames.bind(styles);

function EditInvoice({ data, onClose, className }) {
  const dataRecord = data;
  const intl = useIntl();
  return (
    <Formik
      initialValues={{
        date: dataRecord.date,
        invoiceId: dataRecord.invoiceId,
        htg: dataRecord.htg,
      }}
    >
      {(formik) => (
        <Form className={classNames(className, cx("wrapper"))}>
          <label className={cx("header")}>{"Detail of Invoice"}</label>
          <div className={cx("input-area")}>
            <EditInput
              className={cx("edit-input")}
              name="invoiceId"
              id="invoiceId"
              label={intl.formatMessage({ id: "tbl_invoice" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="date"
              id="date"
              label={intl.formatMessage({ id: "tbl_date_time" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="partnerName"
              id="partnerName"
              label={intl.formatMessage({ id: "tbl_partner" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="quantity"
              id="quantity"
              label={intl.formatMessage({ id: "tbl_quantity" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="status"
              id="status"
              label={intl.formatMessage({ id: "tbl_status" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="file"
              id="file"
              label={intl.formatMessage({ id: "tbl_file" })}
            />
          </div>
          <div className={cx("btn")}>
            <Button
              label={<FormattedMessage id="btn_cancel" />}
              styles={"outline-warn"}
              size={"small"}
              onClick={onClose}
            />
            <Button
              type="submit"
              label={<FormattedMessage id="btn_save" />}
              styles={"outline-ok"}
              size={"small"}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}

export default EditInvoice;
