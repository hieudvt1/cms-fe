import { Form, Formik, Field } from "formik";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import Button from "~/components/Button";
import DayPicker from "../DayPicker";
import styles from "./SearchBar.module.scss";

const cx = classNames.bind(styles);

function SearchBar({ firstCiteria, secondCiteria }) {
  return (
    <Formik
      initialValues={{}}
      onSubmit={(values) => {
        console.log(values);
      }}
    >
      {(formik) => (
        <Form className={cx("wrapper")}>
          <div className={cx("bar")}>
            {firstCiteria.citeria === "day" ? (
              <DayPicker formik={formik} className={cx("day")} />
            ) : (
              firstCiteria.citeria === "input" && (
                <Field
                  type="text"
                  placeholder={firstCiteria.label}
                  name={firstCiteria.name}
                  id={firstCiteria.name}
                  className={cx("input")}
                />
              )
            )}
            {secondCiteria && (
              <>
                <div className={cx("line")}></div>
                <Field
                  type="text"
                  placeholder={secondCiteria.label}
                  name={secondCiteria.name}
                  id={secondCiteria.name}
                  className={cx("input")}
                />
              </>
            )}
          </div>
          <div className={cx("btns")}>
            <Button
              label={<FormattedMessage id="btn_query" />}
              size={"small"}
              styles={"primary"}
              type="submit"
              className={cx("query-btn")}
            />
            <Button
              label={<FormattedMessage id="btn_cancel" />}
              size={"small"}
              styles={"outline-warn"}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}

export default SearchBar;
