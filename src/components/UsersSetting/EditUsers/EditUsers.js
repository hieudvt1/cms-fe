import { Formik, Form } from "formik";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import Button from "~/components/Button";
import EditInput from "./EditInput";
import EditSelect from "./EditSelect";
import styles from "./EditUsers.module.scss";

const cx = classNames.bind(styles);

function EditUsers({ className, onClose, data }) {
  return (
    <Formik initialValues={{}}>
      {(formik) => (
        <Form className={classNames(className, cx("wrapper"))}>
          <label className={cx("header")}>{data.id}</label>
          <div className={cx("input-area")}>
            <EditInput
              className={cx("edit-input")}
              name="userName"
              id="userName"
              label={<FormattedMessage id="user_name" />}
            />
            <EditInput
              className={cx("edit-input")}
              name="password"
              id="password"
              label={<FormattedMessage id="password" />}
            />
            <EditSelect
              className={cx("edit-select")}
              name="type"
              id="type"
              label={<FormattedMessage id="tbl_type" />}
            >
              <option value={"admin"}>Admin</option>
              <option value={"manager"}>Manager</option>
              <option value={"officer"}>Officer</option>
            </EditSelect>
          </div>
          <div className={cx("btn")}>
            <Button
              label={<FormattedMessage id="btn_cancel" />}
              styles={"outline-warn"}
              size={"small"}
              onClick={onClose}
            />
            <Button
              type="submit"
              label={<FormattedMessage id="btn_save" />}
              styles={"outline-ok"}
              size={"small"}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}

export default EditUsers;
