import classNames from "classnames/bind";

import styles from "./EditUsers.module.scss";
import { useField } from "formik";

const cx = classNames.bind(styles);

function EditSelect({ label, placeholder, ...props }) {
  const [field, meta] = useField(props);
  return (
    <div className={cx("edit")}>
      <label htmlFor={props.id || props.name} className={cx("label")}>
        {label}:
      </label>
      <select {...field} {...props} className={cx("select")} />
      <div className={cx("line")}></div>
    </div>
  );
}

export default EditSelect;
