import { forwardRef } from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRightFromBracket,
  faChevronRight,
  faGear,
  faLanguage,
} from "@fortawesome/free-solid-svg-icons";
import { faCircleQuestion } from "@fortawesome/free-regular-svg-icons";
import Tippy from "@tippyjs/react";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import { LOCALES } from "~/i18n/locales";
import { changeLanguage } from "~/features/Language/LanguageSlice";
import styles from "./UserMenu.module.scss";

const cx = classNames.bind(styles);
const menu = [
  { label: <FormattedMessage id="user_language" />, icon: faLanguage },
  {
    label: <FormattedMessage id="user_settting" />,
    icon: faGear,
    path: "/setting",
  },
  {
    label: <FormattedMessage id="user_support" />,
    icon: faCircleQuestion,
    path: "/help",
  },
  {
    label: <FormattedMessage id="user_logout" />,
    icon: faArrowRightFromBracket,
    path: "/logout",
  },
];

const languages = [
  { label: "English", language: LOCALES.ENGLISH },
  { label: "Tiếng Việt", language: LOCALES.VIETNAMESE },
];

const UserItem = forwardRef(({ item }, ref) => (
  <li ref={ref} className={cx("item")}>
    <NavLink to={item.path} className={cx("link")}>
      <FontAwesomeIcon icon={item.icon} className={cx("item-icon")} />
      <label className={cx("item-label")}>{item.label}</label>
      {item.path !== "/logout" && (
        <FontAwesomeIcon icon={faChevronRight} className={cx("item-arrow")} />
      )}
    </NavLink>
  </li>
));

const LanguageItem = ({ item }) => {
  const dispatch = useDispatch();
  const handleChangeLanguage = (locale) => {
    localStorage.setItem("locale", locale);
    dispatch(changeLanguage(locale));
  };
  return (
    <div
      onClick={() => handleChangeLanguage(item.language)}
      className={cx("language-item")}
    >
      {item.label}
    </div>
  );
};

const LanguageMenu = () => (
  <div className={cx("language-menu")}>
    {languages.map((item) => (
      <LanguageItem key={item.label} item={item} />
    ))}
  </div>
);

function UserMenu({ className }) {
  return (
    <ul className={classNames(className, cx("wrapper"))}>
      {menu.map((item, index) =>
        item.path ? (
          <UserItem key={index} item={item} />
        ) : (
          <Tippy
            content={<LanguageMenu />}
            interactive={true}
            placement="left-start"
            offset={[0, 14]}
            key={index}
          >
            <UserItem key={index} item={item} />
          </Tippy>
        )
      )}
    </ul>
  );
}

export default UserMenu;
