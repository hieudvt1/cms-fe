import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";
import Tippy from "@tippyjs/react";

import Image from "~/components/Image";
import UserMenu from "./UserMenu";
import styles from "./User.module.scss";

const cx = classNames.bind(styles);

function User() {
  const [showMenu, setShowMenu] = useState(false);
  return (
    <Tippy
      content={<UserMenu className={cx("menu")} />}
      interactive={true}
      trigger="click"
      hideOnClick={true}
      placement="bottom-start"
      offset={[0, 10]}
    >
      <div className={cx("wrapper")}>
        <Tippy
          content={
            <div className={cx("tooltip")}>
              <FormattedMessage id="user_tooltip" />
            </div>
          }
          placement={"bottom-end"}
        >
          <div onClick={() => setShowMenu(true)} className={cx("content")}>
            <label className={cx("name")}>User Name</label>
            <div className={cx("avatar")}>
              <Image alt="user avatar" className={cx("avatar-img")} />
              <div className={cx("avatar-btn")}>
                <FontAwesomeIcon
                  icon={faChevronDown}
                  className={cx("avatar-icon")}
                />
              </div>
            </div>
          </div>
        </Tippy>
      </div>
    </Tippy>
  );
}

export default User;
