import { NavLink } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import config from "~/config";
import styles from "./Sidebar.module.scss";

const cx = classNames.bind(styles);

const Item = ({ lable }) => (
  <div className={cx("sub-item")}>
    <label className={cx("sub-label")}>{lable}</label>
  </div>
);

function SubItem({ onClick }) {
  return (
    <div className={cx("sub-menu")}>
      <NavLink
        onClick={onClick}
        to={config.routes.contract}
        className={({ isActive }) =>
          cx(isActive ? "active" : "inactive", "sub-link")
        }
      >
        <Item lable={<FormattedMessage id="menu_contract" />} />
      </NavLink>
      <NavLink
        onClick={onClick}
        to={config.routes.connection}
        className={({ isActive }) =>
          cx(isActive ? "active" : "inactive", "sub-link")
        }
      >
        <Item lable={<FormattedMessage id="menu_connect" />} />
      </NavLink>
    </div>
  );
}

export default SubItem;
