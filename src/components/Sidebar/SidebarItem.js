import { forwardRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./Sidebar.module.scss";

const cx = classNames.bind(styles);

function SidebarItem({ itemData, className, forwardRef, onClick }) {
  return (
    <div
      onClick={onClick}
      ref={forwardRef}
      className={classNames(className, cx("item"))}
    >
      <FontAwesomeIcon icon={itemData.icon} className={cx("item-icon")} />
      <label className={cx("item-label")}>{itemData.label}</label>
    </div>
  );
}

export default forwardRef((props, ref) => (
  <SidebarItem forwardRef={ref} {...props} />
));
