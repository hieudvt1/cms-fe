import { useState } from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRightArrowLeft,
  faBars,
  faClockRotateLeft,
  faReceipt,
  faUsers,
  faWarehouse,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage } from "react-intl";
import Tippy from "@tippyjs/react";
import classNames from "classnames/bind";

import config from "~/config";
import SidebarItem from "./SidebarItem";
import SubItem from "./SubItem";
import styles from "./Sidebar.module.scss";

const cx = classNames.bind(styles);

const menu = [
  {
    label: <FormattedMessage id="menu_exchange" />,
    icon: faArrowRightArrowLeft,
    path: config.routes.exchangeRate,
  },
  {
    label: <FormattedMessage id="menu_partner" />,
    icon: faUsers,
    path: config.routes.partner,
  },
  {
    label: <FormattedMessage id="menu_inventory" />,
    icon: faWarehouse,
    path: config.routes.inventory,
  },
  {
    label: <FormattedMessage id="menu_invoice" />,
    icon: faReceipt,
    path: config.routes.invoice,
  },
  {
    label: <FormattedMessage id="menu_history" />,
    icon: faClockRotateLeft,
    path: config.routes.history,
  },
];

function Sidebar({ className }) {
  const [isContract, setIsContract] = useState(false);
  const [showMenu, setShowMenu] = useState(false);
  const [showTippy, setShowTippy] = useState(false);
  const handleClickSidebar = (path) => {
    setShowMenu(false);
    if (path === config.routes.partner) {
      setIsContract(true);
      setShowTippy(false);
    } else {
      setIsContract(false);
    }
  };

  const handleClickMenuButton = () => {
    setShowMenu((prev) => !prev);
  };
  return (
    <div className={classNames(className, cx("wrapper", showMenu && "show"))}>
      <div className={cx("menu-btn")}>
        <FontAwesomeIcon
          onClick={handleClickMenuButton}
          icon={showMenu ? faXmark : faBars}
        />
      </div>
      <div>
        {menu.map((item) =>
          item.path === config.routes.partner ? (
            <Tippy
              content={
                <SubItem onClick={() => handleClickSidebar(item.path)} />
              }
              visible={showTippy}
              interactive={true}
              placement={"right-end"}
              key={item.path}
            >
              <SidebarItem
                itemData={item}
                className={cx(isContract && "active-link")}
                onClick={() => setShowTippy(true)}
              />
            </Tippy>
          ) : (
            <NavLink
              onClick={() => handleClickSidebar(item.path)}
              key={item.path}
              to={item.path}
              className={({ isActive }) =>
                cx(isActive ? "active" : "inactive", "link")
              }
            >
              <SidebarItem itemData={item} />
            </NavLink>
          )
        )}
      </div>
    </div>
  );
}

export default Sidebar;
