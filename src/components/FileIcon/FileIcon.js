import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileLines } from "@fortawesome/free-regular-svg-icons";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames/bind";

import styles from "./FileIcon.module.scss";

const cx = classNames.bind(styles);

function FileIcon({ tick, onClick }) {
  return (
    <div onClick={onClick} className={cx("wrapper")}>
      <FontAwesomeIcon icon={faFileLines} className={cx("file")} />
      {tick && <FontAwesomeIcon icon={faCircleCheck} className={cx("tick")} />}
    </div>
  );
}

export default FileIcon;
