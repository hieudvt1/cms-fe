import { useState } from "react";
import { useField } from "formik";
import { format } from "date-fns";
import { DayPicker as ReactDayPicker } from "react-day-picker";
import "react-day-picker/dist/style.css";
import { faCalendar } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import styles from "./DayPicker.module.scss";
import Tippy from "@tippyjs/react";

const cx = classNames.bind(styles);

function Picker({ formik, ...props }) {
  const [field, meta] = useField(props);
  const [selected, setSelected] = useState();
  const [showPicker, setShowPicker] = useState(false);
  const [dayValue, setDayValue] = useState("");

  let footer = <p>Please pick a day.</p>;
  if (selected) {
    footer = <p>You picked {format(selected, "PP")}.</p>;
  }
  return (
    <Tippy
      content={
        <ReactDayPicker
          className={cx("picker-day")}
          mode="single"
          selected={selected}
          onSelect={setSelected}
          footer={footer}
          onDayClick={(value) => {
            const date = format(value, "dd/MM/yy").toString();
            setDayValue(date);
            formik.setFieldValue(field.name, date);
            setShowPicker(false);
          }}
          modifiersStyles={{
            selected: {
              backgroundColor: "#f26522",
              color: "white",
            },
          }}
        />
      }
      trigger="click"
      interactive={true}
      placement="bottom"
      offset={[0, -20]}
    >
      <div className={cx("picker")}>
        <input
          {...field}
          {...props}
          onChange={(e) => {
            console.log("value:", e.target.value);
            setDayValue(e.target.value);
            formik.handleChange(e);
          }}
          placeholder="dd/mm/yy"
          className={cx("picker-input")}
          value={dayValue}
        />
        <FontAwesomeIcon
          onClick={() => setShowPicker(true)}
          icon={faCalendar}
          className={cx("picker-icon")}
        />
      </div>
    </Tippy>
  );
}

function DayPicker({ formik, className }) {
  return (
    <div className={classNames(className, cx("wrapper"))}>
      <FormattedMessage id="date_from" />
      <Picker name="from" id="from" formik={formik} />
      <FormattedMessage id="date_to" />
      <Picker name="to" id="to" formik={formik} />
    </div>
  );
}

export default DayPicker;
