import DocViewer, { DocViewerRenderers } from "@cyntler/react-doc-viewer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames/bind";

import styles from "./FilePreview.module.scss";

const cx = classNames.bind(styles);

function FilePreview({ onClose, className }) {
  const docs = [
    {
      uri: require("./pdf.pdf"),
    },
  ];
  return (
    <div className={classNames(className, cx("wrapper"))}>
      <FontAwesomeIcon
        onClick={onClose}
        icon={faArrowLeft}
        className={cx("close-btn")}
      />
      <DocViewer
        className={cx("viewer")}
        pluginRenderers={DocViewerRenderers}
        documents={docs}
        config={{
          header: {
            disableHeader: false,
            disableFileName: false,
            retainURLParams: false,
          },
        }}
      />
      {/* <div className={cx("bar")}>
        <div className={cx("function")}></div>
        <Button size={"small"} label={"Download"} styles={"primary"} />
      </div> */}
    </div>
  );
}

export default FilePreview;
