import classNames from "classnames/bind";
import {
  Table as ReactTable,
  Thead,
  Tbody,
  Th,
  Tr,
  Td,
} from "react-super-responsive-table";
import "react-super-responsive-table/dist/SuperResponsiveTableStyle.css";
import SortButton from "~/components/SortButton";
import { useState } from "react";

import styles from "./Table.module.scss";

const cx = classNames.bind(styles);

function Table({
  children,
  header,
  className,
  headerData,
  bodyData,
  selectedRow,
  setSelectedRow,
}) {
  const [data, setData] = useState(bodyData);
  const [sortDirect, setSortDirect] = useState({});

  console.log("data:", data);
  const handleClickSort = (id) => {
    if (!sortDirect[id]) {
      setData(data.sort((a, b) => a[id] - b[id]));
      setSortDirect({ [id]: "up" });
    } else {
      if (sortDirect[id] === "down") {
        setData(data.sort((a, b) => a[id] - b[id]));
        setSortDirect((prev) => ({ ...prev, [id]: "up" }));
      } else {
        setData(data.sort((a, b) => b[id] - a[id]));
        setSortDirect((prev) => ({ ...prev, [id]: "down" }));
      }
    }
  };

  const handleRowClick = (id) => {
    setSelectedRow(id);
  };
  return (
    <div className={classNames(className, cx("wrapper"))}>
      <label className={cx("header")}>{header}</label>
      <ReactTable className={cx("table")}>
        <Thead>
          <Tr>
            {headerData.map((header, index) => (
              <Th key={index}>
                <div className={cx("head-item")}>
                  <label>{header.label}</label>
                  {header.sort && (
                    <SortButton
                      onClick={() => handleClickSort(header.id)}
                      direct={sortDirect[header.id]}
                    />
                  )}
                </div>
              </Th>
            ))}
          </Tr>
        </Thead>
        <Tbody>
          {data.map((dt) => (
            <Tr
              key={dt.id}
              onClick={() => handleRowClick(dt.id)}
              className={cx(selectedRow === dt.id && "selected-row")}
            >
              {Object.values(dt).map((item) => (
                <Td key={item}>{item}</Td>
              ))}
            </Tr>
          ))}
        </Tbody>
      </ReactTable>
    </div>
  );
}

export default Table;
