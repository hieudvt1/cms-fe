import { useState } from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faXmark,
  faBars,
  faGear,
  faSliders,
} from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import SidebarItem from "../Sidebar/SidebarItem";
import styles from "./UserSideBar.module.scss";

const cx = classNames.bind(styles);

const menu = [
  {
    label: <FormattedMessage id="menu_my_setting" />,
    icon: faGear,
    path: "mysetting",
  },
  {
    label: <FormattedMessage id="menu_users_setting" />,
    icon: faSliders,
    path: "userssetting",
  },
];

function UserSideBar({ className }) {
  const [showMenu, setShowMenu] = useState(false);
  const handleClickMenuButton = () => {
    setShowMenu((prev) => !prev);
  };
  return (
    <div className={classNames(className, cx("wrapper", showMenu && "show"))}>
      <div className={cx("menu-btn")}>
        <FontAwesomeIcon
          onClick={handleClickMenuButton}
          icon={showMenu ? faXmark : faBars}
        />
      </div>
      <div>
        {menu.map((item) => (
          <NavLink
            key={item.path}
            to={item.path}
            className={({ isActive }) =>
              cx(isActive ? "active" : "inactive", "link")
            }
          >
            <SidebarItem itemData={item} />
          </NavLink>
        ))}
      </div>
    </div>
  );
}

export default UserSideBar;
