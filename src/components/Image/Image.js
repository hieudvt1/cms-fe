import classNames from "classnames/bind";

import styles from "./Image.module.scss";
import images from "~/assets/images/images";

const cx = classNames.bind(styles);

function Image({ alt, src, className, ...props }) {
  return (
    <img
      alt={alt}
      src={src || images.noImage}
      className={classNames(className)}
    />
  );
}

export default Image;
