import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import Button from "../Button";
import styles from "./ConfirmForm.module.scss";

const cx = classNames.bind(styles);

function ConfirmForm({
  label,
  message,
  onClose,
  className,
  hasButton = true,
  type = "confirm",
}) {
  return (
    <div className={classNames(className, cx("wrapper"))}>
      <FontAwesomeIcon
        onClick={onClose}
        className={cx("back")}
        icon={faArrowLeft}
      />
      {label && <label className={cx("header")}>{label}</label>}
      <p className={cx("message")}>{message}</p>
      {hasButton && type === "confirm" && (
        <Button
          className={cx("btn")}
          label={<FormattedMessage id="btn_confirm" />}
          styles={"primary"}
          size={"small"}
        />
      )}
      {hasButton && type === "update" && (
        <div className={cx("btns")}>
          <Button
            label={<FormattedMessage id="btn_reject" />}
            styles={"outline-warn"}
            size={"small"}
          />
          <Button
            label={<FormattedMessage id="btn_approve" />}
            styles={"outline-ok"}
            size={"small"}
          />
        </div>
      )}
    </div>
  );
}

export default ConfirmForm;
