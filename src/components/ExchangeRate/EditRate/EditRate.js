import { Form, Formik } from "formik";
import { useIntl } from "react-intl";
import classNames from "classnames/bind";

import Button from "~/components/Button";
import EditInput from "./EditInput";
import styles from "./EditRate.module.scss";

const cx = classNames.bind(styles);

function EditRate({ className, onClose, data }) {
  const dataRecord = data;
  const intl = useIntl();
  return (
    <Formik
      initialValues={{
        time: dataRecord.time,
        usd: dataRecord.usd,
        htg: dataRecord.htg,
      }}
    >
      {(formik) => (
        <Form className={classNames(className, cx("wrapper"))}>
          <div className={cx("input-area")}>
            <EditInput
              className={cx("edit-input")}
              name="time"
              id="time"
              label={intl.formatMessage({ id: "tbl_date_time" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="usd"
              id="usd"
              label={"USD"}
            />
            <EditInput
              className={cx("edit-input")}
              name="htg"
              id="htg"
              label={"HTG"}
            />
          </div>
          <div className={cx("btn")}>
            <Button
              label={intl.formatMessage({ id: "btn_cancel" })}
              styles={"outline-warn"}
              size={"small"}
              onClick={onClose}
            />
            <Button
              type="submit"
              label={intl.formatMessage({ id: "btn_save" })}
              styles={"outline-ok"}
              size={"small"}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}

export default EditRate;
