import { useField } from "formik";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import styles from "./LoginInput.module.scss";

const cx = classNames.bind(styles);

function LoginInput({ placeholder, icon, className, ...props }) {
  const [field, meta] = useField(props);
  return (
    <div className={classNames(className, cx("wrapper"))}>
      <div className={cx("input-group")}>
        <input
          placeholder={placeholder}
          {...field}
          {...props}
          className={cx("input")}
        />
        <FontAwesomeIcon icon={icon} className={cx("icon")} />
      </div>
      <div className={cx("error")}>
        {meta.touched && meta.value ? (
          <FormattedMessage id="wrong_input" values={{ m: placeholder }} />
        ) : (
          " "
        )}
      </div>
    </div>
  );
}

export default LoginInput;
