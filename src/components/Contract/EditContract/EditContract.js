import { Formik, Form } from "formik";
import { useIntl } from "react-intl";
import classNames from "classnames/bind";

import Button from "~/components/Button";
import EditInput from "./EditInput";
import styles from "./EditContract.module.scss";

const cx = classNames.bind(styles);

function EditContract({ className, onClose, data }) {
  const contractData = data;
  const intl = useIntl();
  return (
    <Formik initialValues={{ partnerName: contractData.partnerName }}>
      {(formik) => (
        <Form className={classNames(className, cx("wrapper"))}>
          <label className={cx("header")}>{contractData.contractName}</label>
          <div className={cx("input-area")}>
            <EditInput
              className={cx("edit-input")}
              name="partnerName"
              id="partnerName"
              label={intl.formatMessage({ id: "tbl_partner" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="file"
              id="file"
              label={intl.formatMessage({ id: "tbl_file" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="type"
              id="type"
              label={intl.formatMessage({ id: "tbl_type" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="license"
              id="license"
              label={intl.formatMessage({ id: "tbl_license" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="address"
              id="address"
              label={intl.formatMessage({ id: "tbl_address" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="tax"
              id="tax"
              label={intl.formatMessage({ id: "tbl_tax" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="discount"
              id="discount"
              label={intl.formatMessage({ id: "tbl_discount" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="contact"
              id="contact"
              label={intl.formatMessage({ id: "tbl_contact" })}
            />
            <EditInput
              className={cx("edit-input")}
              name="term"
              id="term"
              label={intl.formatMessage({ id: "tbl_term" })}
            />
          </div>
          <div className={cx("btn")}>
            <Button
              label={intl.formatMessage({ id: "btn_cancel" })}
              styles={"outline-warn"}
              size={"small"}
              onClick={onClose}
            />
            <Button
              type="submit"
              label={intl.formatMessage({ id: "btn_save" })}
              styles={"outline-ok"}
              size={"small"}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}

export default EditContract;
