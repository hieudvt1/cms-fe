import { useField } from "formik";
import classNames from "classnames/bind";

import styles from "./EditContract.module.scss";

const cx = classNames.bind(styles);

function EditInput({ label, placeholder, ...props }) {
  const [field, meta] = useField(props);
  return (
    <div className={cx("edit")}>
      <label htmlFor={props.id || props.name} className={cx("label")}>
        {label}:
      </label>
      <input {...field} {...props} className={cx("input")} />
      <div className={cx("line")}></div>
    </div>
  );
}

export default EditInput;
