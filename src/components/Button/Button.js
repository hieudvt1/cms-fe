import classNames from "classnames/bind";

import styles from "./Button.module.scss";

const cx = classNames.bind(styles);

function Button({ label, styles, size, className, type, onClick }) {
  return (
    <button
      onClick={onClick}
      type={type}
      className={classNames(className, cx("wrapper", styles, size))}
    >
      {label}
    </button>
  );
}

export default Button;
