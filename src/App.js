import React from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { IntlProvider } from "react-intl";

import { messages } from "./i18n/messages";
import { LOCALES } from "./i18n/locales";
import LayoutPage from "./pages/LayoutPage";
import Settings from "./pages/Settings/Settings";
import Overlay from "./components/Overlay";
import config from "./config";
import "./App.css";

function App() {
  const locale = useSelector((state) => state.language.language);
  const showOverlay = useSelector((state) => state.overlay.isOpen);
  return (
    <div className="App">
      <BrowserRouter>
        <IntlProvider
          messages={messages[locale]}
          locale={locale}
          defaultLocale={LOCALES.ENGLISH}
        >
          {showOverlay && <Overlay className={"overlay"} />}
          <Routes>
            <Route path={config.routes.home} element={<LayoutPage />}></Route>
            <Route path={config.routes.setting} element={<Settings />}></Route>
          </Routes>
        </IntlProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
