const routes = {
  home: "/*",
  login: "/",
  forgotPass: "/forgotpass",
  exchangeRate: "/exchangerate",
  partner: "/partner",
  contract: "/contract",
  connection: "/connection",
  inventory: "/inventory",
  invoice: "/invoice",
  history: "/history",
  mySetting: "/mysetting",
  usersSetting: "/userssetting",
  setting: "/setting/*",
};

export const config = {
  routes,
};
