import { Routes, Route } from "react-router-dom";
import classNames from "classnames/bind";

import { settingsRoutes } from "~/routers";
import Header from "~/components/Header";
import UserSideBar from "~/components/UserSideBar";
import styles from "./Settings.module.scss";

const cx = classNames.bind(styles);

function Settings() {
  return (
    <div className={cx("wrapper")}>
      <Header />
      <main className={cx("main")}>
        {
          <>
            <UserSideBar className={cx("sidebar")} />
            <div className={cx("content")}>
              <Routes>
                {settingsRoutes.map((route) => (
                  <Route
                    key={route.path}
                    path={route.path}
                    element={<route.component />}
                  />
                ))}
              </Routes>
            </div>
          </>
        }
      </main>
    </div>
  );
}

export default Settings;
