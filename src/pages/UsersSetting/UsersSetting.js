import { useState } from "react";
import { useDispatch } from "react-redux";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import { setOverlay } from "~/features/overlay/overlaySlice";
import SearchBar from "~/components/SearchBar";
import Table from "~/components/Table";
import Button from "~/components/Button";
import EditUsers from "~/components/UsersSetting/EditUsers/";
import ConfirmForm from "~/components/ConfirmForm";
import styles from "./UsersSetting.module.scss";

const cx = classNames.bind(styles);

const headers = [
  { label: <FormattedMessage id="tbl_no" />, id: "id", sort: true },
  { label: <FormattedMessage id="tbl_username" />, id: "userName", sort: true },
  {
    label: <FormattedMessage id="tbl_create_date" />,
    id: "createDate",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_type" />, id: "type", sort: true },
];

const initData = [
  { id: 0, userName: "ABC", createDate: "22/09/89", type: "Admin" },
  { id: 1, userName: "ABC", createDate: "22/09/89", type: "Office" },
  { id: 2, userName: "ABC", createDate: "22/09/89", type: "Manager" },
];

function UsersSetting() {
  const [selectedRow, setSelectedRow] = useState();
  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const dispatch = useDispatch();
  const intl = useIntl();

  const handleClickAdd = () => {
    dispatch(setOverlay(true));
    setShowAdd(true);
  };

  const handleCancelAdd = () => {
    dispatch(setOverlay(false));
    setShowAdd(false);
  };
  const handleClickEdit = () => {
    dispatch(setOverlay(true));
    setShowEdit(true);
  };

  const handleCancelEdit = () => {
    dispatch(setOverlay(false));
    setShowEdit(false);
  };
  const handleClickDelete = () => {
    dispatch(setOverlay(true));
    setShowDelete(true);
  };

  const handleCancelDelete = () => {
    dispatch(setOverlay(false));
    setShowDelete(false);
  };
  return (
    <div className={cx("wrapper")}>
      <SearchBar
        firstCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_user_type" }),
          name: "contract",
        }}
        secondCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_user_name" }),
          name: "partner",
        }}
      />
      <Table
        header={<FormattedMessage id="lbl_user_management" />}
        headerData={headers}
        bodyData={initData}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
        className={cx("table")}
      ></Table>
      <div className={cx("btns")}>
        <Button
          onClick={handleClickAdd}
          size={"medium"}
          styles={"primary"}
          label={<FormattedMessage id="btn_add_user" />}
        />
        <Button
          onClick={selectedRow !== null && handleClickEdit}
          size={"medium"}
          styles={"outline"}
          label={<FormattedMessage id="btn_edit_user" />}
        />
        <Button
          size={"medium"}
          styles={"outline-warn"}
          label={<FormattedMessage id="btn_delete_user" />}
          onClick={selectedRow !== null && handleClickDelete}
        />
      </div>
      {showAdd && (
        <EditUsers
          data={[]}
          onClose={handleCancelAdd}
          className={cx("edit-form")}
        />
      )}
      {showEdit && (
        <EditUsers
          data={initData.filter((data) => data.id === selectedRow)[0]}
          onClose={handleCancelEdit}
          className={cx("edit-form")}
        />
      )}
      {showDelete && (
        <ConfirmForm
          className={cx("delete-form")}
          label={initData.filter((data) => data.id === selectedRow)[0].userName}
          message={<FormattedMessage id="delete_user" />}
          onClose={handleCancelDelete}
        />
      )}
    </div>
  );
}

export default UsersSetting;
