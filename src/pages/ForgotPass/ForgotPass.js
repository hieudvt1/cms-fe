import { FormattedMessage, useIntl } from "react-intl";
import { Formik, Form } from "formik";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import classNames from "classnames/bind";

import LoginInput from "~/components/Login/LoginInput/";
import Button from "~/components/Button/";
import styles from "./ForgotPass.module.scss";

const cx = classNames.bind(styles);

function ForgotPass() {
  const intl = useIntl();
  return (
    <Formik
      initialValues={{ email: "" }}
      onSubmit={(values) => {
        console.log(values);
      }}
    >
      {(formik) => (
        <Form className={cx("wrapper")}>
          <label className={cx("label")}>
            <FormattedMessage id="forgot_password" />
          </label>
          <label className={cx("desc")}>
            <FormattedMessage id="email_enter" />
          </label>
          <LoginInput
            name="email"
            id="email"
            type="email"
            icon={faEnvelope}
            placeholder={intl.formatMessage({ id: "email" })}
            className={cx("email")}
          />

          <Button
            label={intl.formatMessage({ id: "btn_send_password" })}
            type={"submit"}
            size={"large"}
            styles={"primary"}
            className={cx("btn")}
          />
        </Form>
      )}
    </Formik>
  );
}

export default ForgotPass;
