import { useState } from "react";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExport } from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import { setOverlay } from "~/features/overlay/overlaySlice";
import SearchBar from "~/components/SearchBar";
import Button from "~/components/Button";
import Table from "~/components/Table";
import FileIcon from "~/components/FileIcon";
import EditInvoice from "~/components/Invoice/EditInvoice/EditInvoice";
import FilePreview from "~/components/FilePreview";
import ConfirmForm from "~/components/ConfirmForm";
import styles from "./Invoice.module.scss";

const cx = classNames.bind(styles);

const headers = [
  { label: <FormattedMessage id="tbl_no" />, id: "id", sort: true },
  {
    label: <FormattedMessage id="tbl_partner" />,
    id: "partnerName",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_date" />, id: "date", sort: true },
  { label: <FormattedMessage id="tbl_invoice" />, id: "invoiceId", sort: true },
  { label: <FormattedMessage id="tbl_file" />, id: "file", sort: false },
  { label: <FormattedMessage id="tbl_value" />, id: "value", sort: true },
  { label: <FormattedMessage id="tbl_status" />, id: "status", sort: true },
  { label: <FormattedMessage id="tbl_export" />, id: "export", sort: false },
];

const initData = [
  {
    id: 0,
    partnerName: "Abc",
    date: "22/05/24",
    invoiceId: 135,
    file: true,
    value: 34.5,
    status: "",
  },
  {
    id: 1,
    partnerName: "Abc",
    date: "22/05/24",
    invoiceId: 137,
    file: true,
    value: 34.5,
    status: "",
  },
  {
    id: 2,
    partnerName: "Abc",
    date: "22/05/24",
    invoiceId: 133,
    file: true,
    value: 34.5,
    status: "",
  },
];

function Invoice() {
  const [selectedRow, setSelectedRow] = useState();
  const [showEdit, setShowEdit] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [showPreview, setShowPreview] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [showExport, setShowExport] = useState(false);
  const [showUpdate, setShowUpdate] = useState(false);
  const intl = useIntl();
  const dispatch = useDispatch();

  const handleClickAdd = () => {
    dispatch(setOverlay(true));
    setShowAdd(true);
  };

  const handleCancelAdd = () => {
    dispatch(setOverlay(false));
    setShowAdd(false);
  };

  const handleClickEdit = () => {
    dispatch(setOverlay(true));
    setShowEdit(true);
  };

  const handleCancelEdit = () => {
    dispatch(setOverlay(false));
    setShowEdit(false);
  };

  const handleClickUpdate = () => {
    dispatch(setOverlay(true));
    setShowUpdate(true);
  };

  const handleCloseUpdate = () => {
    dispatch(setOverlay(false));
    setShowUpdate(false);
  };

  const handleClickDelete = () => {
    dispatch(setOverlay(true));
    setShowDelete(true);
  };

  const handleCloseDelete = () => {
    dispatch(setOverlay(false));
    setShowDelete(false);
  };

  initData.forEach((data) => {
    data.export = (
      <FontAwesomeIcon
        onClick={() => setShowExport(true)}
        className={cx("export-btn")}
        icon={faFileExport}
      />
    );
    data.file = <FileIcon onClick={() => setShowPreview(true)} />;
  });
  return (
    <div className={cx("wrapper")}>
      <SearchBar
        firstCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_contract_type" }),
          name: "contract",
        }}
        secondCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_partner_name" }),
          name: "partner",
        }}
      />
      <Button
        size={"medium"}
        styles={"outline"}
        label={<FormattedMessage id="btn_export_all" />}
        className={cx("export-all")}
      />
      <Table
        header={<FormattedMessage id="menu_invoice" />}
        headerData={headers}
        bodyData={initData}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
        className={cx("table")}
      ></Table>
      <div className={cx("btns")}>
        <Button
          size={"medium"}
          styles={"primary"}
          label={<FormattedMessage id="btn_add_invoice" />}
          onClick={handleClickAdd}
        />
        <Button
          onClick={selectedRow !== null && handleClickEdit}
          size={"medium"}
          styles={"outline"}
          label={<FormattedMessage id="btn_edit_invoice" />}
        />
        <Button
          size={"medium"}
          styles={"outline"}
          label={<FormattedMessage id="btn_update_status" />}
          onClick={selectedRow !== null && handleClickUpdate}
        />
        <Button
          size={"medium"}
          styles={"outline-warn"}
          label={<FormattedMessage id="btn_delete_invoice" />}
          onClick={selectedRow !== null && handleClickDelete}
        />
      </div>
      {showAdd && (
        <EditInvoice
          data={[]}
          onClose={handleCancelAdd}
          className={cx("edit-form")}
        />
      )}
      {showPreview && (
        <FilePreview
          onClose={() => setShowPreview(false)}
          className={cx("viewer")}
        />
      )}
      {showEdit && (
        <EditInvoice
          data={initData.filter((data) => data.id === selectedRow)[0]}
          onClose={handleCancelEdit}
          className={cx("edit-form")}
        />
      )}
      {showDelete && (
        <ConfirmForm
          className={cx("delete-form")}
          label={
            initData.filter((data) => data.id === selectedRow)[0].invoiceId
          }
          message={<FormattedMessage id="delete_invoice" />}
          onClose={handleCloseDelete}
        />
      )}
      {showExport && (
        <ConfirmForm
          className={cx("export-form")}
          label={
            initData.filter((data) => data.id === selectedRow)[0].invoiceId
          }
          message={<FormattedMessage id="export_invoice" />}
          onClose={() => setShowExport(false)}
        />
      )}
      {showUpdate && (
        <ConfirmForm
          className={cx("update-form")}
          label={
            initData.filter((data) => data.id === selectedRow)[0].invoiceId
          }
          message={<FormattedMessage id="approve_invoice" />}
          onClose={handleCloseUpdate}
          type="update"
        />
      )}
    </div>
  );
}

export default Invoice;
