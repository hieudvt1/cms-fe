import { useState } from "react";
import { useDispatch } from "react-redux";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import { setOverlay } from "~/features/overlay/overlaySlice";
import SearchBar from "~/components/SearchBar";
import Table from "~/components/Table";
import Button from "~/components/Button";
import FileIcon from "~/components/FileIcon";
import FilePreview from "~/components/FilePreview";
import EditContract from "~/components/Contract/EditContract";
import ConfirmForm from "~/components/ConfirmForm";
import styles from "./Contract.module.scss";

const cx = classNames.bind(styles);

const headers = [
  { label: <FormattedMessage id="tbl_no" />, id: "id", sort: true },
  {
    label: <FormattedMessage id="tbl_partner" />,
    id: "partnerName",
    sort: true,
  },
  {
    label: <FormattedMessage id="tbl_contract_name" />,
    id: "contractName",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_file" />, id: "file", sort: false },
  {
    label: <FormattedMessage id="tbl_sign_date" />,
    id: "signDate",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_license" />, id: "license", sort: false },
  { label: <FormattedMessage id="tbl_type" />, id: "type", sort: true },
  { label: <FormattedMessage id="tbl_status" />, id: "status", sort: true },
  { label: <FormattedMessage id="tbl_detail" />, id: "btn", sort: false },
];

const initData = [
  {
    id: 0,
    partnerName: "Abc",
    contractName: "Xyz Contract5",
    file: false,
    signDate: "22/04/23",
    license: true,
    type: "",
    status: "",
  },
  {
    id: 1,
    partnerName: "Abd",
    contractName: "Xyz Contract3",
    file: true,
    signDate: "22/04/23",
    license: true,
    type: "",
    status: "",
  },
  {
    id: 2,
    partnerName: "Abe",
    contractName: "Xyz Contract2",
    file: true,
    signDate: "22/04/23",
    license: true,
    type: "",
    status: "",
  },
];

function Contract() {
  const [showPreview, setShowPreview] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showUpdate, setShowUpdate] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [selectedRow, setSelectedRow] = useState(null);
  const intl = useIntl();
  const dispatch = useDispatch();

  const handleClickEdit = () => {
    dispatch(setOverlay(true));
    setShowEdit(true);
  };

  const handleCancelEdit = () => {
    dispatch(setOverlay(false));
    setShowEdit(false);
  };

  const handleClickAdd = () => {
    dispatch(setOverlay(true));
    setShowAdd(true);
  };

  const handleCancelAdd = () => {
    dispatch(setOverlay(false));
    setShowAdd(false);
  };

  const handleClickUpdate = () => {
    dispatch(setOverlay(true));
    setShowUpdate(true);
  };

  const handleCancelUpdate = () => {
    dispatch(setOverlay(false));
    setShowUpdate(false);
  };

  const handleClickDelete = () => {
    dispatch(setOverlay(true));
    setShowDelete(true);
  };

  const handleCloseDelete = () => {
    dispatch(setOverlay(false));
    setShowDelete(false);
  };

  initData.forEach((data) => {
    data.btn = (
      <Button
        label={<FormattedMessage id="btn_update" />}
        size={"small"}
        styles={"outline"}
        onClick={handleClickUpdate}
      />
    );
    data.file = (
      <FileIcon tick={data.file} onClick={() => setShowPreview(true)} />
    );
  });

  return (
    <div className={cx("wrapper")}>
      <SearchBar
        firstCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_contract_type" }),
          name: "contract",
        }}
        secondCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_partner_type" }),
          name: "partner",
        }}
      />
      <Table
        header={<FormattedMessage id="menu_contract" />}
        headerData={headers}
        bodyData={initData}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
        className={cx("table")}
      ></Table>
      <div className={cx("btns")}>
        <Button
          size={"medium"}
          styles={"primary"}
          label={<FormattedMessage id="btn_add_contract" />}
          onClick={handleClickAdd}
        />
        <Button
          size={"medium"}
          styles={"outline"}
          label={<FormattedMessage id="btn_edit_contract" />}
          onClick={selectedRow !== null && handleClickEdit}
        />
        <Button
          size={"medium"}
          styles={"outline-warn"}
          label={<FormattedMessage id="btn_delete_contract" />}
          onClick={() => {
            selectedRow !== null && handleClickDelete();
          }}
        />
      </div>
      {showAdd && (
        <EditContract
          data={[]}
          onClose={handleCancelAdd}
          className={cx("edit-form")}
        />
      )}
      {showPreview && (
        <FilePreview
          onClose={() => setShowPreview(false)}
          className={cx("viewer")}
        />
      )}
      {showUpdate && (
        <EditContract
          data={initData.filter((data) => data.id === selectedRow)[0]}
          onClose={handleCancelUpdate}
          className={cx("edit-form")}
        />
      )}
      {showEdit && (
        <EditContract
          data={initData.filter((data) => data.id === selectedRow)[0]}
          onClose={handleCancelEdit}
          className={cx("edit-form")}
        />
      )}
      {showDelete && (
        <ConfirmForm
          className={cx("delete-form")}
          label={
            initData.filter((data) => data.id === selectedRow)[0].contractName
          }
          message={<FormattedMessage id="delete_contract" />}
          onClose={handleCloseDelete}
        />
      )}
    </div>
  );
}

export default Contract;
