import { Field, Form, Formik } from "formik";
import { faUser } from "@fortawesome/free-regular-svg-icons";
import { faUnlockKeyhole } from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import LoginInput from "~/components/Login/LoginInput";
import Button from "~/components/Button/Button";
import styles from "./Login.module.scss";

const cx = classNames.bind(styles);

function Login() {
  const intl = useIntl();
  return (
    <Formik
      initialValues={{ name: "", password: "", remember: false }}
      onSubmit={(values) => {
        console.log(values);
      }}
    >
      {(formik) => (
        <Form className={cx("wrapper")}>
          <label className={cx("label")}>
            <FormattedMessage id={"acc_login"} />
          </label>
          <LoginInput
            name="name"
            id="name"
            type="text"
            placeholder={intl.formatMessage({ id: "user_name" })}
            icon={faUser}
            className={cx("name")}
          />
          <LoginInput
            name="password"
            id="password"
            type="password"
            placeholder={intl.formatMessage({ id: "password" })}
            icon={faUnlockKeyhole}
            className={cx("password")}
          />
          <div className={cx("option")}>
            <div className={cx("remember")}>
              <Field
                type="checkbox"
                name="remember"
                id="remember"
                className={cx("checkbox")}
              />
              <label>
                <FormattedMessage id="remember_me" />
              </label>
            </div>
            <a href="/#" className={cx("forgotpassword")}>
              <FormattedMessage id="forgot_password_link" />
            </a>
          </div>
          <Button
            label={intl.formatMessage({ id: "btn_login" })}
            type={"submit"}
            size={"large"}
            styles={"primary"}
            className={cx("btn")}
          />
        </Form>
      )}
    </Formik>
  );
}

export default Login;
