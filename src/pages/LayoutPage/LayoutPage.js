import { Navigate, Route, Routes } from "react-router-dom";
import classNames from "classnames/bind";

import config from "~/config";
import Header from "~/components/Header";
import Sidebar from "~/components/Sidebar";
import { publicRoutes, privateRoutes } from "~/routers";
import styles from "./LayoutPage.module.scss";

const cx = classNames.bind(styles);

const user = true;

function LayoutPage() {
  return (
    <div className={cx("wrapper")}>
      <Header />
      <main className={cx("main")}>
        {user ? (
          <>
            <Sidebar className={cx("sidebar")} />
            <div className={cx("content")}>
              <Routes>
                {privateRoutes.map((route) => (
                  <Route
                    key={route.path}
                    path={route.path}
                    element={<route.component />}
                  />
                ))}
                <Route
                  path="/*"
                  element={<Navigate to={config.routes.login} />}
                />
              </Routes>
            </div>
          </>
        ) : (
          <Routes>
            {publicRoutes.map((route) => (
              <Route
                key={route.path}
                path={route.path}
                element={<route.component />}
              />
            ))}
          </Routes>
        )}
      </main>
    </div>
  );
}

export default LayoutPage;
