import { Field, Form, Formik } from "formik";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import Button from "~/components/Button";
import Image from "~/components/Image";
import styles from "./MySetting.module.scss";

const cx = classNames.bind(styles);

function MySetting() {
  const intl = useIntl();
  return (
    <div className={cx("wrapper")}>
      <Formik initialValues={{ name: "" }}>
        {(formik) => (
          <Form className={cx("user-info")}>
            <div className={cx("info")}>
              <Image className={cx("avatar")} />
              <div className={cx("name")}>
                <div className={cx("input")}>
                  <label htmlFor={"name"} className={cx("label-name")}>
                    <FormattedMessage id="user_name" />
                  </label>
                  <Field
                    name={"name"}
                    id={"name"}
                    className={cx("name-input")}
                  />
                </div>
                <Button
                  styles={"outline"}
                  size={"medium"}
                  label={<FormattedMessage id="btn_upload_avatar" />}
                  className={cx("upload-btn")}
                />
              </div>
            </div>
            <Button
              type={"submit"}
              styles={"primary"}
              size={"medium"}
              label={<FormattedMessage id="btn_save_changes" />}
              className={cx("save-btn")}
            />
          </Form>
        )}
      </Formik>
      <Formik>
        {(formik) => (
          <Form className={cx("password")}>
            <Field
              placeholder={intl.formatMessage({ id: "password" })}
              className={cx("pass-input")}
            />
            <Field
              placeholder={intl.formatMessage({ id: "new_pass" })}
              className={cx("pass-input")}
            />
            <Field
              placeholder={intl.formatMessage({ id: "confirm_pass" })}
              className={cx("pass-input")}
            />
            <Button
              type={"submit"}
              styles={"primary"}
              size={"medium"}
              label={<FormattedMessage id="btn_change_pass" />}
              className={cx("change-btn")}
            />
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default MySetting;
