import { useState } from "react";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExport } from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import { setOverlay } from "~/features/overlay/overlaySlice";
import SearchBar from "~/components/SearchBar";
import Table from "~/components/Table";
import Button from "~/components/Button";
import styles from "./Inventory.module.scss";
import ConfirmForm from "~/components/ConfirmForm";

const cx = classNames.bind(styles);

const headers = [
  { label: <FormattedMessage id="tbl_no" />, id: "id", sort: true },
  {
    label: <FormattedMessage id="tbl_partner" />,
    id: "partnerName",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_date" />, id: "date", sort: true },
  { label: <FormattedMessage id="tbl_invoice" />, id: "invoiceId", sort: true },
  {
    label: <FormattedMessage id="tbl_open_stock" />,
    id: "openStock",
    sort: true,
  },
  {
    label: <FormattedMessage id="tbl_close_stock" />,
    id: "closeStock",
    sort: true,
  },
  {
    label: <FormattedMessage id="tbl_current_stock" />,
    id: "currentStock",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_export" />, id: "export", sort: false },
];

const initData = [
  {
    id: 0,
    partnerName: "Abc",
    date: "22/02/22",
    invoiceId: 231,
    openStock: 123,
    closeStock: 584,
    currentStock: 48,
  },
  {
    id: 1,
    partnerName: "Abc",
    date: "22/02/22",
    invoiceId: 2314,
    openStock: 123,
    closeStock: 584,
    currentStock: 48,
  },
  {
    id: 2,
    partnerName: "Abc",
    date: "22/02/22",
    invoiceId: 2313,
    openStock: 123,
    closeStock: 584,
    currentStock: 48,
  },
];

function Inventory() {
  const [selectedRow, setSelectedRow] = useState(null);
  const [showExport, setShowExport] = useState(false);
  const [showExportAll, setShowExportAll] = useState(false);
  const dispatch = useDispatch();

  const handleClickExport = () => {
    dispatch(setOverlay(true));
    setShowExport(true);
  };

  const handleCloseExport = () => {
    dispatch(setOverlay(false));
    setShowExport(false);
  };

  const handleClickExportAll = () => {
    dispatch(setOverlay(true));
    setShowExportAll(true);
  };

  const handleCloseExportAll = () => {
    dispatch(setOverlay(false));
    setShowExportAll(false);
  };

  initData.forEach((data) => {
    data.export = (
      <FontAwesomeIcon
        onClick={handleClickExport}
        className={cx("export-btn")}
        icon={faFileExport}
      />
    );
  });

  return (
    <div className={cx("wrapper")}>
      <SearchBar
        firstCiteria={{
          citeria: "day",
        }}
        secondCiteria={{
          citeria: "input",
          label: "Type of the partner",
          name: "partner",
        }}
      />
      <Button
        size={"medium"}
        styles={"outline"}
        label={<FormattedMessage id="btn_export_all" />}
        className={cx("export-all")}
        onClick={handleClickExportAll}
      />
      <Table
        header={<FormattedMessage id="menu_inventory" />}
        headerData={headers}
        bodyData={initData}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
        className={cx("table")}
      ></Table>

      {showExport && (
        <ConfirmForm
          className={cx("export-form")}
          onClose={handleCloseExport}
          label={
            initData.filter((data) => data.id === selectedRow)[0].invoiceId
          }
          message={<FormattedMessage id="export_inventory" />}
        />
      )}
      {showExportAll && (
        <ConfirmForm
          className={cx("export-form")}
          onClose={handleCloseExportAll}
          label={<FormattedMessage id="export_all" />}
          message={<FormattedMessage id="export_inventory" />}
        />
      )}
    </div>
  );
}

export default Inventory;
