import { useState } from "react";
import { useDispatch } from "react-redux";
import { FormattedMessage } from "react-intl";
import classNames from "classnames/bind";

import { setOverlay } from "~/features/overlay/overlaySlice";
import SearchBar from "~/components/SearchBar";
import Table from "~/components/Table";
import Button from "~/components/Button";
import EditRate from "~/components/ExchangeRate/EditRate/EditRate";
import styles from "./ExchangeRate.module.scss";

const cx = classNames.bind(styles);
const headers = [
  { label: <FormattedMessage id="tbl_no" />, id: "id", sort: true },
  { label: <FormattedMessage id="tbl_date_time" />, id: "time", sort: true },
  { label: "USD", id: "usd", sort: true },
  { label: "HTG", id: "htg", sort: true },
  { label: "", id: "btn", sort: false },
];

const initData = [
  { id: 0, time: 423, usd: 1, htg: 1.5 },
  { id: 1, time: 323, usd: 1, htg: 1.8 },
  { id: 2, time: 123, usd: 1, htg: 1.2 },
];

function ExchangeRate() {
  const [showEdit, setShowEdit] = useState(false);
  const [selectedRow, setSelectedRow] = useState(null);
  const dispatch = useDispatch();
  const handleClickUpdate = () => {
    dispatch(setOverlay(true));
    setShowEdit(true);
  };
  const handleCloseUpdate = () => {
    dispatch(setOverlay(false));
    setShowEdit(false);
  };
  initData.forEach((data) => {
    data.btn = (
      <Button
        label={<FormattedMessage id="btn_update" />}
        size={"small"}
        styles={"outline"}
        onClick={handleClickUpdate}
      />
    );
  });

  return (
    <div className={cx("wrapper")}>
      <SearchBar firstCiteria={{ citeria: "day" }} />
      <Table
        header={<FormattedMessage id="menu_exchange" />}
        headerData={headers}
        bodyData={initData}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
        className={cx("table")}
      ></Table>
      {showEdit && (
        <EditRate
          data={initData.filter((data) => data.id === selectedRow)[0]}
          onClose={handleCloseUpdate}
          className={cx("edit-form")}
        />
      )}
    </div>
  );
}

export default ExchangeRate;
