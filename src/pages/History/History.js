import { useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import classNames from "classnames/bind";

import SearchBar from "~/components/SearchBar";
import Button from "~/components/Button";
import Table from "~/components/Table";
import styles from "./History.module.scss";

const cx = classNames.bind(styles);

const headers = [
  { label: <FormattedMessage id="tbl_no" />, id: "id", sort: true },
  {
    label: <FormattedMessage id="tbl_trading_code" />,
    id: "tradingCode",
    sort: true,
  },
  { label: "MISDN", id: "misdn", sort: true },
  { label: <FormattedMessage id="tbl_date" />, id: "date", sort: true },
  {
    label: <FormattedMessage id="tbl_partner" />,
    id: "partnerName",
    sort: true,
  },
  { label: <FormattedMessage id="tbl_package" />, id: "package", sort: true },
  { label: "USD", id: "usd", sort: true },
  { label: "HTG", id: "htg", sort: true },
  {
    label: <FormattedMessage id="tbl_exchange_rate" />,
    id: "exchangeRate",
    sort: true,
  },
  {
    label: <FormattedMessage id="tbl_account_balance" />,
    id: "accountBalance",
    sort: true,
  },
  {
    label: <FormattedMessage id="tbl_error_code" />,
    id: "errorCode",
    sort: true,
  },
];

const initData = [
  {
    id: 123,
    tradingCode: 5784,
    misdn: 853,
    date: "22/09/34",
    partnerName: "ADB",
    package: "jfdkljf",
    usd: 1,
    htg: 3.5,
    exchangeRate: 3.6,
    accountBalance: 345,
    errorCode: 3894,
  },
  {
    id: 124,
    tradingCode: 5784,
    misdn: 853,
    date: "22/09/34",
    partnerName: "ADB",
    package: "jfdkljf",
    usd: 1,
    htg: 3.5,
    exchangeRate: 3.6,
    accountBalance: 345,
    errorCode: 3894,
  },
];

function History() {
  const [selectedRow, setSelectedRow] = useState();
  const intl = useIntl();
  return (
    <div className={cx("wrapper")}>
      <SearchBar
        firstCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "tbl_error_code" }),
          name: "error",
        }}
        secondCiteria={{
          citeria: "input",
          label: intl.formatMessage({ id: "ipt_partner_type" }),
          name: "partner",
        }}
      />

      <Table
        header={<FormattedMessage id="menu_history" />}
        headerData={headers}
        bodyData={initData}
        className={cx("table")}
        selectedRow={selectedRow}
        setSelectedRow={setSelectedRow}
      ></Table>
    </div>
  );
}

export default History;
