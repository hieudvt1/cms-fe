const images = {
  logo: require("./The_Natcom_Logo.png"),
  noImage: require("./No-Image-Placeholder.svg.png"),
};

export default images;
